#include "request.h"

Request::Request()
{
}

int Request::idRequest() const
{
    return _idRequest;
}

void Request::setIdRequest(int id)
{
    _idRequest = id;
}


int Request::idClient() const
{
    return _idClient;
}


void Request::setIdClient(int id)
{
    _idClient = id;
}

QString Request::requestDate() const
{
    return _requestDate;
}

void Request::setRequestDate(const QString &date)
{
    _requestDate = date;
}

