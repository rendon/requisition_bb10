#include "RequisitionBB10.hpp"

#include <bb/cascades/Application>
#include <Qt/qdeclarativedebug.h>
#include "backend.h"

Q_DECL_EXPORT int main(int argc, char **argv)
{
    bb::cascades::Application app(argc, argv);
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForCStrings(codec);

    new RequisitionBB10(&app);

    return bb::cascades::Application::exec();
}

