#ifndef REQUEST_H
#define REQUEST_H
#include <QString>

class Request
{
public:
    Request();
    int idRequest() const;
    void setIdRequest(int id);

    int idClient() const;
    void setIdClient(int id);

    QString requestDate() const;
    void setRequestDate(const QString &date);

private:
    int _idRequest;
    int _idClient;
    QString _requestDate;
};

#endif // REQUEST_H
