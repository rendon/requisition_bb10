#ifndef CLIENT_H
#define CLIENT_H
#include <QString>

class Client
{
public:
    Client();
    Client(int id, const QString &n, const QString m,
           const QString &l, const QString &a);

    int idClient() const;
    void setIdClient(int id);

    QString name() const;
    void setName(const QString &value);

    QString middleName() const;
    void setMiddleName(const QString &value);

    QString lastName() const;
    void setLastName(const QString &value);

    QString address() const;
    void setAddress(const QString &value);

private:
    int _idClient;
    QString _name;
    QString _middleName;
    QString _lastName;
    QString _address;

};

#endif // CLIENT_H
