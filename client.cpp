#include "client.h"

Client::Client()
{
}

Client::Client(int id, const QString &n, const QString m,
               const QString &l, const QString &a)
{
    _idClient = id;
    _name = n;
    _middleName = m;
    _lastName = l;
    _address = a;
}


int Client::idClient() const
{
    return _idClient;
}

void Client::setIdClient(int id)
{
    _idClient = id;
}

QString Client::name() const
{
    return _name;
}

void Client::setName(const QString &value)
{
    _name = value;
}

QString Client::middleName() const
{
    return _middleName;
}

void Client::setMiddleName(const QString &value)
{
    _middleName = value;
}

QString Client::lastName() const
{
    return _lastName;
}

void Client::setLastName(const QString &value)
{
    _lastName = value;
}

QString Client::address() const
{
    return _address;
}

void Client::setAddress(const QString &value)
{
    _address = value;
}
