#ifndef BACKEND_H
#define BACKEND_H
#include <QObject>
#include <QMetaType>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QUrl>
#include <QVector>
#include <QtSql/QSqlQuery>
#include <QDebug>
#include <bb/cascades/QListDataModel>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/DropDown>
#include <bb/cascades/ProgressIndicator>
#include <bb/cascades/Label>
#include <bb/data/SqlDataAccess>
#include <bb/system/SystemToast>
using namespace bb::cascades;
using namespace bb::data;
using namespace bb::system;

#include "client.h"
#include "request.h"
#include "detail.h"
#include "product.h"
#include "key.h"

const QString WS_URL("http://192.168.1.200/request_ws/index.php");
const QString MSG_GET_ALL_CLIENTS =
        "<soapenv:Envelope "
        "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' "
        "xmlns:req='Request'> <soapenv:Header/> <soapenv:Body> "
        "<req:GetAllClients/> </soapenv:Body> </soapenv:Envelope>";

const QString MSG_GET_ALL_PRODUCTS =
        "<soapenv:Envelope "
        "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' "
        "xmlns:req='Request'> <soapenv:Header/> <soapenv:Body> "
        "<req:GetAllProducts/> </soapenv:Body> </soapenv:Envelope>";

const QString MSG_GET_ALL_REQUESTS =
        "<soapenv:Envelope "
        "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' "
        "xmlns:req='Request'> <soapenv:Header/> <soapenv:Body> "
        "<req:GetAllRequests/> </soapenv:Body> </soapenv:Envelope>";

const QString MSG_GET_ALL_DETAILS =
        "<soapenv:Envelope "
        "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' "
        "xmlns:req='Request'> <soapenv:Header/> <soapenv:Body> "
        "<req:GetAllDetails/> </soapenv:Body> </soapenv:Envelope>";

const QString DB_PATH = "./data/Pedidos.db";

const QString CREATE_TABLE_CLIENT =
        "CREATE TABLE IF NOT EXISTS Client (IdClient "
        "INTEGER PRIMARY KEY,Name VARCHAR(20), "
        "MiddleName VARCHAR(20), LastName VARCHAR(20), "
        "Address VARCHAR(100))";

const QString CREATE_TABLE_PRODUCT =
        "CREATE TABLE IF NOT EXISTS Product(IdProduct "
        "INTEGER PRIMARY KEY, Description VARCHAR(100))";

const QString CREATE_TABLE_REQUEST =
        "CREATE TABLE IF NOT EXISTS Request(IdRequest "
        "INTEGER PRIMARY KEY AUTOINCREMENT, "
        "IdClient INTEGER, RequestDate DATETIME, "
        "FOREIGN KEY (IdClient) "
        "REFERENCES Client (IdClient))";

const QString REQUEST_UNIQUE_CONSTRAINT =
        "CREATE UNIQUE INDEX IF NOT EXISTS unique_request ON "
        "Request(IdRequest, IdClient)";

const QString CREATE_TABLE_DETAIL =
        "CREATE TABLE IF NOT EXISTS Detail("
        "IdDetail INTEGER PRIMARY KEY AUTOINCREMENT, "
        "IdRequest INTEGER, IdProduct INTEGER, "
        "Amount INTEGER, FOREIGN KEY (IdRequest) "
        "REFERENCES Request (IdRequest), "
        "FOREIGN KEY(IdProduct) "
        "REFERENCES Product(IdProduct))";

const QString DETAIL_UNIQUE_CONSTRAINT =
        "CREATE UNIQUE INDEX IF NOT EXISTS unique_detail "
        "ON Detail(IdDetail, IdRequest)";

class Backend : public QObject
{
    Q_OBJECT

public:
    Backend(QObject *parent = 0);
    Backend(SystemToast *toast);
    virtual ~Backend();
    Q_INVOKABLE void queryClients();
    Q_INVOKABLE void queryProducts();

    Q_INVOKABLE bool saveRequest(QString idClient);
    Q_INVOKABLE bool addDetail(QString idProduct, QString product, QString amount);
    Q_INVOKABLE void removeDatabase();
    Q_INVOKABLE void loadClientsToList();
    Q_INVOKABLE void loadProductsToList();

    Q_INVOKABLE void loadRequestsToList();
    Q_INVOKABLE void putRegisters();
    Q_INVOKABLE void updateCatalogs();

    QVector<Client> toClientArray(QString message);
    QVector<Product> toProductArray(QString message);
    QVector<Request> toRequestArray(QString message);
    QVector<Detail> toDetailArray(QString message);

    QVector<Client> selectClients();
    QVector<Product> selectProducts();

    GroupDataModel *newDetailsModel() const;
    void setNewDetailsModel(GroupDataModel *newDetailsModel);

    GroupDataModel *requestModel() const;
    void setRequestModel(GroupDataModel *requestsModel);

    GroupDataModel *productsModel() const;
    void setProductsModel(GroupDataModel *productsModel);

    DropDown *productList() const;
    void setProductList(DropDown *productList);

    DropDown *clientList() const;
    void setClientList(DropDown *clientList);

    ProgressIndicator *progressIndicator() const;
    void setProgressIndicator(ProgressIndicator *progressIndicator);

    Label *progressMessage() const;
    void setProgressMessage(Label *progressMessage);

public slots:
    void getClientsFinished(QNetworkReply *reply);
    void getProductsFinished(QNetworkReply *reply);
    void getRequestsFinished(QNetworkReply *reply);
    void getDetailsFinished(QNetworkReply *reply);
    void putRequestsFinished(QNetworkReply *reply);

private:
    GroupDataModel *_newDetailsModel;
    GroupDataModel *_requestModel;
    QVector<Detail> *_newDetails;

    DropDown *_productList;
    DropDown *_clientList;
    ProgressIndicator *_progressIndicator;
    Label *_progressMessage;

    bool validInteger(QString number);
    bool initDataBase();
    bool insertClient(int id, QString name, QString middleName,
                      QString lastName, QString address);
    bool insertProduct(int id, QString description);
    int insertRequest(int idClient, QString date);
    bool insertDetail(int idRequest, int product, int amount);

    QString formattedRegisters();

    QVector<Request> selectRequests();
    QVector<Detail> selectDetails();

    SqlDataAccess *sda;

    SystemToast *toast;
    inline QString to_s(int n);
};

#endif // BACKEND_H
