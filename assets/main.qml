import bb.cascades 1.0
import bb.system 1.0

TabbedPane {
    showTabsOnActionBar: true
    Tab {
        title: "Inicio"
        imageSource: "asset:///pictures/home.png"
        NavigationPane {
            id: homeNavigationPane
            objectName: "homeNavigationPane"
            Page {
                id: home
                Container {
                    layout: DockLayout { }

                    Container {
                        verticalAlignment: VerticalAlignment.Top
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }

                        ImageView {
                            imageSource: "asset:///pictures/LogoITCH.png"
                        }

                        Label {
                            multiline: true
                            leftMargin: 50
                            textStyle {
                                fontSize: FontSize.Large
                            }
                            text:   "Desarrollo de Aplicaciones\npara Tecnologías Móviles"
                        }
                    }

                    Container {
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Fill

                        layout: StackLayout { }

                        Button {
                            text: "Enviar registros al servidor"
                            horizontalAlignment: HorizontalAlignment.Fill
                            onClicked: {
                                homeNavigationPane.push(progressPage)
                                backend.putRegisters()
                            }
                        }

                        Button {
                            text: "Limpiar registros y actualizar catalogos"
                            horizontalAlignment: HorizontalAlignment.Fill
                            onClicked: {
                                homeNavigationPane.push(progressPage)
                                backend.updateCatalogs()
                            }
                        }

                    }


                }
            }

        }
    }

    Tab {
        title: "Pedidos"
        imageSource: "asset:///pictures/shop.png"

        NavigationPane {
            id: requestNavigationPane
            objectName: "requestNavigationPane"
            Page {
                Container {
                    layout: StackLayout { }

                    DropDown {
                        horizontalAlignment: HorizontalAlignment.Fill
                        title: "Cliente"
                        objectName: "clientList"
                        id: clientList
                        enabled: true

                    }

                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 4
                        }

                        ListView {
                            objectName: "newItems"
                            listItemComponents: ListItemComponent {
                                type: "item"

                                Container {
                                    Container {
                                        layout: StackLayout {
                                            orientation: LayoutOrientation.LeftToRight
                                        }

                                        Label {
                                            layoutProperties: StackLayoutProperties {
                                                spaceQuota: 1
                                            }

                                            text: ListItemData.amount
                                        }

                                        Label {
                                            layoutProperties: StackLayoutProperties {
                                                spaceQuota: 6
                                            }

                                            text: ListItemData.description
                                        }

                                    }

                                    Divider {
                                        horizontalAlignment: HorizontalAlignment.Fill
                                    }
                                }

                            }
                        }
                    }

                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }

                        DropDown {
                            objectName: "productList"
                            id: productList
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 6
                            }
                            title: "Producto"
                            enabled: true

                        }

                        TextField {
                            id: itemAmount
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 2
                            }
                            hintText: "Cantidad"
                        }

                        Button {
                            attachedObjects: [
                                SystemToast {
                                    id: productToast
                                    body: "Seleciona un producto."
                                }
                            ]

                            imageSource:"asset:///pictures/add.png"
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            onClicked: {
                                var selectedOption = productList.selectedOption
                                if (selectedOption  !== null) {
                                    var idProduct = selectedOption.value
                                    var description = selectedOption.text
                                    console.log("IdProduct = " + idProduct)
                                    var ok = backend.addDetail(idProduct, description, itemAmount.text)
                                    if (ok) {
                                        itemAmount.text = ""
                                        productList.resetSelectedOption()
                                    }
                                } else {
                                    productToast.show()
                                }
                            }
                        }
                    }

                    Button {
                        attachedObjects: [
                            SystemToast {
                                id: clientToast
                                body: "No hay nada que guardar."
                            }
                        ]

                        horizontalAlignment: HorizontalAlignment.Fill
                        text: "Guardar pedido"
                        onClicked: {
                            var selectedOption = clientList.selectedOption
                            if (selectedOption !== null) {
                                var idClient = selectedOption.value
                                console.log("IdClient = " + idClient)
                                var ok = backend.saveRequest(idClient)
                                if (ok) {
                                    idClient.text = ""
                                    clientList.resetSelectedOption()
                                }
                            } else {
                                clientToast.show()
                            }
                        }
                    }
                }
            }
        }
    }

    Tab {
        imageSource: "asset:///pictures/search.png"
        Page {
            id: queryPage
            paneProperties: NavigationPaneProperties {
                backButton: ActionItem {
                    onTriggered: {
                        requestNavigationPane.pop()
                    }
                }
            }

            Container {
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }

                    Label {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 5
                        }

                        text: "Lista de pedidos"
                    }

                    Button {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1
                        }
                        imageSource: "asset:///pictures/refresh.png"
                        onClicked: {
                            backend.loadRequestsToList()
                        }
                    }

                }

                ListView {
                    objectName: "listOfRequests"
                    listItemComponents: ListItemComponent {
                        type: "item"

                        Container {
                            layout: StackLayout { }

                            Container {
                                layout: StackLayout { orientation: LayoutOrientation.LeftToRight }
                                Label {
                                    layoutProperties: StackLayoutProperties { spaceQuota: 1 }
                                }

                                Label {
                                    layoutProperties: StackLayoutProperties { spaceQuota: 1 }
                                    text: ListItemData.amount
                                }

                                Label {
                                    layoutProperties: StackLayoutProperties { spaceQuota: 6 }
                                    text: ListItemData.description
                                }
                            }

                            Divider {
                                horizontalAlignment: HorizontalAlignment.Fill
                            }
                        }
                    }
                }
            }
        }
    }

}


