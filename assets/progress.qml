import bb.cascades 1.0

Page {
    id: progressPage
    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            onTriggered: {
                homeNavigationPane.pop()
            }
        }
    }

    Container {
        layout: DockLayout { }
        ProgressIndicator {
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            objectName: "progressIndicator"
            fromValue: 0
            toValue: 100
            value: 0
        }

        Label {
            objectName: "progressMessage"
            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Center
            text: ""
        }
    }
}
