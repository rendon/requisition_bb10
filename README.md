Requisition Black Berry 10
==========================
A simple Black Berry 10 App to control buy's requests. This application is coupled with my other project [Request_WS](https://bitbucket.org/rendon/request_ws).

For more information see the project write up (in Spanish): [http://rendon.x10.mx/?p=888](http://rendon.x10.mx/?p=888).

License
=======
This work is under GPLv3.
