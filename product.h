#ifndef PRODUCT_H
#define PRODUCT_H
#include <QString>

class Product
{
public:
    Product();

    int idProduct() const;
    void setIdProduct(int idProduct);

    QString description() const;
    void setDescription(const QString &description);

private:
    int _idProduct;
    QString _description;

};

#endif // PRODUCT_H
