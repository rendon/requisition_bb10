TEMPLATE = app

LIBS += -lbbdata -lbb -lbbcascades -lbbsystem
QT += declarative xml network

SOURCES += main.cpp \
    RequisitionBB10.cpp \
    client.cpp \
    detail.cpp \
    request.cpp \
    backend.cpp \
    product.cpp

HEADERS += RequisitionBB10.hpp \
    client.h \
    detail.h \
    request.h \
    backend.h \
    product.h

OTHER_FILES += bar-descriptor.xml \
    assets/main.qml \
    assets/home.png \
    assets/client.png \
    assets/shop.png \
    assets/progress.qml

RESOURCES +=

