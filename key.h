#ifndef KEY_H
#define KEY_H

class Key
{
public:
    Key();
    Key(int pk, int idc, int idr, int idd);

    int newIdClient() const;
    void setNewIdClient(int newIdClient);

    int newIdRequest() const;
    void setNewIdRequest(int newIdRequest);

    int newIdDetail() const;
    void setNewIdDetail(int newIdDetail);

    int pk() const;
    void setPk(int pk);

private:
    int _pk;
    int _newIdClient;
    int _newIdRequest;
    int _newIdDetail;
};

#endif // KEY_H
