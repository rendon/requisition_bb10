#ifndef DETAIL_H
#define DETAIL_H
#include <QString>

class Detail
{
public:
    Detail();
    Detail(int idd, int idr, int idp, int a);

    int idDetail() const;
    void setIdDetail(int id);

    int idRequest() const;
    void setIdRequest(int id);

    int amount() const;
    void setAmount(int value);

    int idProduct() const;
    void setIdProduct(int product);

private:
    int _idDetail;
    int _idRequest;
    int _idProduct;
    int _amount;
};

#endif // DETAIL_H
