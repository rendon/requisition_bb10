#ifndef REQUISITIONBB10_HPP_
#define REQUISITIONBB10_HPP_

#include <QObject>

namespace bb { namespace cascades { class Application; }}

class RequisitionBB10 : public QObject
{
    Q_OBJECT
public:
    RequisitionBB10(bb::cascades::Application *app);
    virtual ~RequisitionBB10() {}
};

#endif /* REQUISITIONBB10_HPP_ */

