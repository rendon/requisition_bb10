#include "backend.h"

Backend::Backend(QObject *parent) : QObject(parent) { }

Backend::Backend(SystemToast *toast)
{
    _newDetails = new QVector<Detail>();
    _newDetailsModel = new GroupDataModel(this);
    _requestModel = new GroupDataModel(QStringList() << "Request");
    _requestModel->setGrouping(ItemGrouping::ByFullValue);
    initDataBase();

    this->toast = toast;
}

Backend::~Backend()
{
    delete _newDetails;
    delete _newDetailsModel;
    delete _requestModel;
}

void Backend::queryClients()
{
    QUrl url(WS_URL);
    QNetworkAccessManager *networkManager = new QNetworkAccessManager(this);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "text/xml");
    QObject::connect(networkManager, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(getClientsFinished(QNetworkReply*)));
    networkManager->post(request, MSG_GET_ALL_CLIENTS.toAscii());
}

void Backend::queryProducts()
{
    QUrl url(WS_URL);
    QNetworkAccessManager *networkManager = new QNetworkAccessManager(this);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "text/xml");
    QObject::connect(networkManager, SIGNAL(finished(QNetworkReply *)),
                     this, SLOT(getProductsFinished(QNetworkReply*)));
    networkManager->post(request, MSG_GET_ALL_PRODUCTS.toAscii());
}

bool Backend::addDetail(QString idProduct, QString description, QString amount)
{
    if (!validInteger(amount))
        return false;

    Detail detail(0, 0, idProduct.toInt(), amount.toInt());
    _newDetails->append(detail);

    QVariantMap entry;
    entry["amount"] = amount;
    entry["description"] = description;
    _newDetailsModel->insert(entry);

    return true;
}

bool Backend::saveRequest(QString idClient)
{
    int idc = idClient.toInt();

    if (_newDetails->isEmpty()) {
        toast->setBody("Debe haber al menos un artículo.");
        toast->show();
        return false;
    }

    QDate date = QDate::currentDate();
    QTime time = QTime::currentTime();
    QString year    = to_s(date.year());
    QString month   = to_s(date.month());
    QString day     = to_s(date.day());
    QString hour    = to_s(time.hour());
    QString minute  = to_s(time.minute());
    QString second  = to_s(time.second());
    QString rightNow = year + "-" + month + "-" + day + " "
                     + hour + ":" + minute + ":" + second;

    int idRequest = insertRequest(idc, rightNow);

    if (idRequest == -1)
        return false;

    int n = _newDetails->size();
    for (int i = 0; i < n; i++) {
        int idProduct = _newDetails->at(i).idProduct();
        int amount = _newDetails->at(i).amount();
        if (!insertDetail(idRequest, idProduct, amount))
            return false;
    }

    toast->setBody("¡Exito!");
    toast->show();
    _newDetails->clear();
    _newDetailsModel->clear();
    return true;
}

void Backend::removeDatabase()
{
    QSqlDatabase db = QSqlDatabase::database(DB_PATH, false);
    db.close();
    QSqlDatabase::removeDatabase(DB_PATH);
    QFile file(DB_PATH);
    file.remove();
}

void Backend::loadClientsToList()
{
    QVector<Client> clients = selectClients();
    _clientList->removeAll();
    int n = clients.size();
    for (int i = 0; i < n; i++) {
        Option *op = new Option();
        op->setText(clients[i].name() + " "
                  + clients[i].middleName() + ""
                  + clients[i].lastName());
        op->setValue(clients[i].idClient());
        _clientList->add(op);
    }
}

void Backend::loadProductsToList()
{
    QVector<Product> products = selectProducts();
    _productList->removeAll();
    int n = products.size();
    for (int i = 0; i < n; i++) {
        Option *op = new Option();
        op->setText(products[i].description());
        op->setValue(products[i].idProduct());
        _productList->add(op);
    }

    _progressMessage->setText("¡Actualización completa!");
    _progressIndicator->setValue(100);
}

void Backend::loadRequestsToList()
{
    QString query = "SELECT Request.IdRequest, Client.Name, Client.MiddleName, "
                    "Client.LastName, Product.Description, Detail.Amount, "
                    "Request.RequestDate FROM Request, Client, Detail, Product "
                    "WHERE Detail.IdRequest = Request.IdRequest AND "
                    "Request.IdClient = Client.IdClient AND "
                    "Detail.IdProduct = Product.IdProduct";
    QVariant result = sda->execute(query);
    if (sda->hasError()) {
        qDebug() << "SOME ERROR: "  << sda->error() << endl;
    }

    QVariantList list = result.value<QVariantList>();

    int n = list.size();
    _requestModel->clear();

    QVariantMap entry;
    QVariantMap row;
    for (int i = 0; i < n; i++) {
        row = list.at(i).value<QVariantMap>();
        QString id = row["IdRequest"].toString();
        QString client  = row["Name"].toString() + " "
                        + row["MiddleName"].toString() + " "
                        + row["LastName"].toString();
        QString date = row["RequestDate"].toString();

        entry["Request"] = id + ": " + client + " (" + date + ")";

        entry["description"] = row["Description"].toString();
        entry["amount"] = row["Amount"].toString();

        _requestModel->insert(entry);
    }
}

void Backend::putRegisters()
{
    QUrl url(WS_URL);
    QNetworkAccessManager *networkManager = new QNetworkAccessManager(this);
    QNetworkRequest request(url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "text/xml");
    QObject::connect(networkManager, SIGNAL(finished(QNetworkReply *)),
            this, SLOT(putRequestsFinished(QNetworkReply*)));
    networkManager->post(request, formattedRegisters().toAscii());
    _progressMessage->setText("Enviando registros...");
    _progressIndicator->setValue(50);
}

void Backend::updateCatalogs()
{
    removeDatabase();
    _progressMessage->setText("Limpiando registros");
    _progressIndicator->setValue(25);
    initDataBase();

    _progressMessage->setText("Descargando catalogo de clientes...");
    _progressIndicator->setValue(50);
    queryClients();
    _progressMessage->setText("Descargando catalogo de clientes...");
    _progressIndicator->setValue(75);
    queryProducts();
}


QString Backend::formattedRegisters()
{
    QString header = "<soapenv:Envelope "
                     "xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/'"
                     " xmlns:req='Request'> <soapenv:Header/> <soapenv:Body> "
                     " <req:PutRegisters>";

    QString footer = "</req:PutRegisters> </soapenv:Body> </soapenv:Envelope>";

    QString requests = "\n";
    requests += "<RequestItems>";
    requests += "\n";

    QVector<Request> R = selectRequests();
    int n = R.size();
    for (int i = 0; i < n; i++) {
        requests += "<item>";
        requests += "<IdRequest>" + to_s(R[i].idRequest()) + "</IdRequest>";
        requests += "<IdClient>" + to_s(R[i].idClient())+ "</IdClient>";
        requests += "<RequestDate>" + R[i].requestDate() + "</RequestDate>";
        requests += "</item>";
        requests += "\n";
    }
    requests += "</RequestItems>";

    QString details = "<DetailItems>";
    details += "\n";
    QVector<Detail> D = selectDetails();
    n = D.size();
    for (int i = 0; i < n; i++) {
        details += "<item>";
        details += "<IdDetail>" + to_s(D[i].idDetail()) + "</IdDetail>";
        details += "<IdRequest>" + to_s(D[i].idRequest()) + "</IdRequest>";
        details += "<IdProduct>" + to_s(D[i].idProduct()) + "</IdProduct>";
        details += "<Amount>" + to_s(D[i].amount()) + "</Amount>";
        details += "</item>";
        details += "\n";
    }
    details += "</DetailItems>";


    QString message = header + requests + "\n" + details + "\n" + footer;
    return message;
}

void Backend::getClientsFinished(QNetworkReply *reply)
{
    QByteArray ans = reply->readAll();
    QString response(ans.data());
    QVector<Client> clients = toClientArray(response);
    int n = clients.size();
    for (int i = 0; i < n; i++) {
        insertClient(clients[i].idClient(),
                     clients[i].name(),
                     clients[i].middleName(),
                     clients[i].lastName(),
                     clients[i].address());
    }

    loadClientsToList();
}

void Backend::getProductsFinished(QNetworkReply *reply)
{
    QByteArray ans = reply->readAll();
    QString response(ans.data());
    QVector<Product> products = toProductArray(response);

    int n = products.size();
    for (int i = 0; i < n; i++) {
        insertProduct(products[i].idProduct(), products[i].description());
    }

    loadProductsToList();
}

void Backend::getRequestsFinished(QNetworkReply *reply)
{
    QByteArray ans = reply->readAll();
    QString response(ans.data());
    QVector<Request> requests = toRequestArray(response);
}

void Backend::getDetailsFinished(QNetworkReply *reply)
{
    QByteArray ans = reply->readAll();
    QString response(ans.data());
    QVector<Detail> details = toDetailArray(response);

}

void Backend::putRequestsFinished(QNetworkReply *reply)
{
    QString response = reply->readAll().data();
    _progressMessage->setText("¡Los registros han sido enviados!");
    _progressIndicator->setValue(100);
}

Label *Backend::progressMessage() const
{
    return _progressMessage;
}

void Backend::setProgressMessage(Label *progressMessage)
{
    _progressMessage = progressMessage;
}

ProgressIndicator *Backend::progressIndicator() const
{
    return _progressIndicator;
}

void Backend::setProgressIndicator(ProgressIndicator *progressIndicator)
{
    _progressIndicator = progressIndicator;
}

DropDown *Backend::clientList() const
{
    return _clientList;
}

void Backend::setClientList(DropDown *clientList)
{
    _clientList = clientList;
}

DropDown *Backend::productList() const
{
    return _productList;
}

void Backend::setProductList(DropDown *productList)
{
    _productList = productList;
}

GroupDataModel *Backend::requestModel() const
{
    return _requestModel;
}

void Backend::setRequestModel(GroupDataModel *requestModel)
{
    _requestModel = requestModel;
}

bool Backend::validInteger(QString number)
{
    QRegExp regex("[1-9]\\d{0,3}");
    int pos = 0;
    QRegExpValidator validator(regex, 0);
    return validator.validate(number, pos) == QRegExpValidator::Acceptable;
}

QVector<Client> Backend::toClientArray(QString message)
{
    QVector<Client> list;
    Client client;
    QXmlStreamReader reader(message);
    while (!reader.atEnd()) {
        reader.readNext();
        QString name = reader.name().toString();
        if (name == "IdClient" || name == "Name" || name == "MiddleName" ||
                name == "LastName" || name == "Address") {
            QString value = reader.readElementText();

            if (name == "IdClient") {
                client.setIdClient(value.toInt());
            } else if (name == "Name") {
                client.setName(value);
            } else if (name == "MiddleName") {
                client.setMiddleName(value);
            } else if (name == "LastName") {
                client.setLastName(value);
            } else if (name == "Address") {
                client.setAddress(value);
                list.append(client);
            }
        }
    }

    return list;
}

QVector<Product> Backend::toProductArray(QString message)
{
    QVector<Product> list;
    Product product;
    QXmlStreamReader reader(message);
    while (!reader.atEnd()) {
        reader.readNext();
        QString name = reader.name().toString();
        if (name == "IdProduct" || name == "Description") {
            QString value = reader.readElementText();

            if (name == "IdProduct") {
                product.setIdProduct(value.toInt());
            } else if (name == "Description") {
                product.setDescription(value);
                list.append(product);
            }
        }
    }

    return list;
}

QVector<Request> Backend::toRequestArray(QString message)
{
    QVector<Request> list;
    Request request;
    QXmlStreamReader reader(message);
    while (!reader.atEnd()) {
        reader.readNext();
        QString name = reader.name().toString();
        if (name == "IdRequest" || name == "IdClient" || name == "RequestDate"){
            QString value = reader.readElementText();

            if (name == "IdRequest") {
                request.setIdRequest(value.toInt());
            } else if (name == "IdClient") {
                request.setIdClient(value.toInt());
            } else if (name == "RequestDate") {
                request.setRequestDate(value);
                list.append(request);
            }
        }
    }

    return list;
}

QVector<Detail> Backend::toDetailArray(QString message)
{
    QVector<Detail> list;
    Detail detail;
    QXmlStreamReader reader(message);
    while (!reader.atEnd()) {
        reader.readNext();
        QString name = reader.name().toString();
        if (name == "IdDetail" || name == "IdRequest" ||
                name == "Item" || name == "Amount") {
            QString value = reader.readElementText();

            if (name == "IdDetail") {
                detail.setIdDetail(value.toInt());
            } else if (name == "IdRequest") {
                detail.setIdRequest(value.toInt());
            } else if (name == "Item") {
                detail.setIdProduct(value.toInt());
            } else if (name == "Amount") {
                detail.setAmount(value.toInt());
                list.append(detail);
            }
        }
    }

    return list;
}


GroupDataModel *Backend::newDetailsModel() const
{
    return _newDetailsModel;
}

bool Backend::initDataBase()
{
    //removeDatabase(); return true;
    QSqlDatabase database = QSqlDatabase::addDatabase("QSQLITE");
    database.setConnectOptions("foreign_key_constraints=ON");
    database.setDatabaseName(DB_PATH);

    if (!database.open()) {
        qDebug() << "ERROR: Could NOT open the database :(" << endl;
        return false;
    }

    this->sda = new SqlDataAccess(DB_PATH);
    sda->execute("PRAGMA foreign_keys = ON"); // FORCE foreign key constraints

    if (sda->hasError())
        qDebug() << "ERROR: " << sda->error() << endl;

    sda->execute(CREATE_TABLE_CLIENT);
    if (sda->hasError()) {
        qDebug() << "ERROR: " << sda->error() << endl;
        return false;
    }

    sda->execute(CREATE_TABLE_REQUEST);
    sda->execute(REQUEST_UNIQUE_CONSTRAINT);

    if (sda->hasError()) {
        qDebug() << "ERROR: " << sda->error() << endl;
        return false;
    }

    sda->execute(CREATE_TABLE_PRODUCT);
    if (sda->hasError()) {
        qDebug() << "ERROR: " << sda->error() << endl;
        return false;
    }

    sda->execute(CREATE_TABLE_DETAIL);
    sda->execute(DETAIL_UNIQUE_CONSTRAINT);
    if (sda->hasError()) {
        qDebug() << "ERROR: " << sda->error() << endl;
        return false;
    }

    return true;
}

bool Backend::insertClient(int id, QString name, QString middleName,
                           QString lastName, QString address)
{
    QVariantList client;
    client << id << name << middleName << lastName << address;
    QString values = QString("VALUES(%1, '%2', '%3', '%4', '%5')")
                     .arg(to_s(id), name, middleName, lastName, address);

    QString query = "INSERT INTO Client(IdClient, Name, MiddleName, "
                    " LastName, Address) " + values;

    sda->execute(query);
    if (sda->hasError()) {
        qDebug() << "ERROR: " << sda->error() << endl;
        return false;
    }

    return true;
}

bool Backend::insertProduct(int id, QString description)
{
    QString values = QString("VALUES(%1, '%2')").arg(to_s(id), description);
    sda->execute("INSERT INTO PRODUCT(IdProduct, Description) " + values);

    if (sda->hasError()) {
        qDebug() << "ERROR::: " << sda->error() << endl;
        return false;
    }

    return true;
}

int Backend::insertRequest(int idClient, QString date)
{
    QString values = QString("VALUES(%1, '%2')").arg(to_s(idClient), date);
    QString query = "INSERT INTO Request(IdClient, RequestDate) " + values;
    QSqlQuery Q = sda->connection().exec(query);
    //qDebug() << "LAST INSERTED ID: " << Q.lastInsertId().toInt() << endl;

    if (sda->hasError()) {
        qDebug() << "ERROR: " << sda->error() << endl;
        return -1;
    }

    return Q.lastInsertId().toInt();
}

bool Backend::insertDetail(int idRequest, int idProduct, int amount)
{
    QString idr = to_s(idRequest);
    QString a = to_s(amount);
    QString p = to_s(idProduct);

    QString values = QString("VALUES(%1, %2, %3)").arg(idr, p, a);
    sda->execute("INSERT INTO Detail(IdRequest, IdProduct, Amount) " + values);

    if (sda->hasError()) {
        qDebug() << "ERROR: " << sda->error() << endl;
        return false;
    }

    return true;
}


QVector<Client> Backend::selectClients()
{
    QString query = "SELECT * FROM Client";
    QVariant result = sda->execute(query);
    QVariantList list = result.value<QVariantList>();
    int n = list.size();
    QVector<Client> clients;
    for (int i = 0; i < n; i++) {
        Client c;
        QVariantMap map = list.at(i).value<QVariantMap>();
        c.setIdClient(map["IdClient"].toString().toInt());
        c.setName(map["Name"].toString());
        c.setMiddleName(map["MiddleName"].toString());
        c.setLastName(map["LastName"].toString());
        c.setAddress(map["Address"].toString());
        clients.append(c);
    }
    return clients;
}

QVector<Product> Backend::selectProducts()
{
    QString query = "SELECT * FROM Product";
    QVariant result = sda->execute(query);
    QVariantList list = result.value<QVariantList>();
    int n = list.size();
    QVector<Product> products;
    for (int i = 0; i < n; i++) {
        Product p;
        QVariantMap map = list.at(i).value<QVariantMap>();
        p.setIdProduct(map["IdProduct"].toString().toInt());
        p.setDescription(map["Description"].toString());
        products.append(p);
    }

    return products;
}

QVector<Request> Backend::selectRequests()
{
    QString query = "SELECT * FROM Request";
    QVariant result = sda->execute(query);
    QVariantList list = result.value<QVariantList>();
    int n = list.size();
    QVector<Request> requests;
    for (int i = 0; i < n; i++) {
        Request r;
        QVariantMap map = list.at(i).value<QVariantMap>();
        r.setIdRequest(map["IdRequest"].toString().toInt());
        r.setIdClient(map["IdClient"].toString().toInt());
        r.setRequestDate(map["RequestDate"].toString());
        requests.append(r);
    }

    return requests;
}

QVector<Detail> Backend::selectDetails()
{
    QString query = "SELECT * FROM Detail";
    QVariant result = sda->execute(query);
    QVariantList list = result.value<QVariantList>();
    int n = list.size();
    QVector<Detail> details;
    for (int i = 0; i < n; i++) {
        QVariantMap map = list.at(i).value<QVariantMap>();
        Detail d;
        d.setIdDetail(map["IdDetail"].toString().toInt());
        d.setIdRequest(map["IdRequest"].toString().toInt());
        d.setIdProduct(map["IdProduct"].toString().toInt());
        d.setAmount(map["Amount"].toString().toInt());

        details.append(d);
    }

    return details;
}


QString Backend::to_s(int n)
{
    return QString::number(n);
}
