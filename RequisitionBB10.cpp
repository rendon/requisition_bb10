#include "RequisitionBB10.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/ListView>
#include <bb/cascades/Page>
#include <bb/cascades/NavigationPane>
#include <bb/cascades/DropDown>
#include <bb/cascades/Option>
#include <bb/cascades/Label>
#include <bb/cascades/ProgressIndicator>

using namespace bb::cascades;

#include "backend.h"
#include "client.h"

RequisitionBB10::RequisitionBB10(Application *app) : QObject(app)
{
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);
    AbstractPane *root = qml->createRootObject<AbstractPane>();

    SystemToast *toast = new SystemToast(this);
    Backend *backend = new Backend(toast);

    ListView *newItems = root->findChild<ListView*>("newItems");
    newItems->setDataModel(backend->newDetailsModel());

    DropDown *productList = root->findChild<DropDown*>("productList");
    backend->setProductList(productList);

    DropDown *clientList = root->findChild<DropDown*>("clientList");
    backend->setClientList(clientList);
    qml->setContextProperty("backend", backend);

    ListView *listOfRequests = root->findChild<ListView*>("listOfRequests");
    listOfRequests->setDataModel(backend->requestModel());

    QmlDocument *qml_progress = QmlDocument::create("asset:///progress.qml");
    Page *progressPage = qml_progress->createRootObject<Page>();
    qml->setContextProperty("progressPage", progressPage);
    NavigationPane *homeNP = root->findChild<NavigationPane*>("homeNavigationPane");
    qml_progress->setContextProperty("homeNavigationPane", homeNP);

    ProgressIndicator *indicator = progressPage->findChild<ProgressIndicator*>("progressIndicator");
    backend->setProgressIndicator(indicator);
    Label *progressMessage = progressPage->findChild<Label*>("progressMessage");
    backend->setProgressMessage(progressMessage);
    backend->loadClientsToList();
    backend->loadProductsToList();

    app->setScene(root);
}


