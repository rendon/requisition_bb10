#include "detail.h"

Detail::Detail()
{
}

Detail::Detail(int idd, int idr, int idp, int a)
{
    _idDetail = idd;
    _idRequest = idr;
    _idProduct = idp;
    _amount = a;
}

int Detail::idDetail() const
{
    return _idDetail;
}

void Detail::setIdDetail(int id)
{
    _idDetail = id;
}

int Detail::idRequest() const
{
    return _idRequest;
}

void Detail::setIdRequest(int id)
{
    _idRequest = id;
}


int Detail::amount() const
{
    return _amount;
}

void Detail::setAmount(int value)
{
    _amount = value;
}
int Detail::idProduct() const
{
    return _idProduct;
}

void Detail::setIdProduct(int idProduct)
{
    _idProduct = idProduct;
}

